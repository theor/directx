#pragma once
#include "Core\Entity.h"

class Cube : public Entity
{
public:
	Cube();
	~Cube(void);
	virtual void LoadContent(LPDIRECT3DDEVICE9 device);
	virtual void Update(unsigned int ticks);
	virtual void Draw( LPDIRECT3DDEVICE9 device );
};

