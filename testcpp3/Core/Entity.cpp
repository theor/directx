#include "StdAfx.h"
#include "Entity.h"
#include "Inputs.h"

Entity::Entity(void)
{
}


Entity::~Entity(void)
{
	_buffer->Release();
}

void Entity::LoadContent(LPDIRECT3DDEVICE9 device)
{
	_device = device;
	UpdateMatrix();
}

void Entity::Update( unsigned int ticks )
{
	_device->SetTransform(D3DTS_WORLD, &( _rotationMatrix * _world ));
	UpdateMatrix();

}


void Entity::UpdateMatrix()
{
	D3DXMatrixTranslation(&_world, _position.x, _position.y	, _position.z);
	D3DXMatrixRotationYawPitchRoll(&_rotationMatrix, _rotation.x, _rotation.y, _rotation.z);
}
