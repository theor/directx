#include "StdAfx.h"
#include "Camera.h"


Camera::Camera(D3DXVECTOR3 pos, D3DXVECTOR3 lookat)
{
	_up = D3DXVECTOR3 (0.0f, 1.0f, 0.0f);

	_position = pos;
	_lookat = lookat;

	D3DXMatrixPerspectiveFovLH(&_projection,
		D3DXToRadian(45),    // the horizontal field of view
		(FLOAT)800.0f / (FLOAT)600.0f,    // aspect ratio
		1.0f,    // the near view-plane
		100.0f);    // the far view-plane

	UpdateMatrix();
}


Camera::~Camera(void)
{
}

void
Camera::Update( unsigned int ticks )
{

}

void Camera::UpdateMatrix()
{
	D3DXMatrixLookAtLH(&_view, &_position, &_lookat, &_up);

}
