#pragma once
#include "../stdafx.h"


class Entity
{
public:
	Entity(void);
	~Entity(void);
	virtual void LoadContent(LPDIRECT3DDEVICE9 device);
	virtual void Draw(LPDIRECT3DDEVICE9 device) = 0;
	virtual void Update(unsigned int ticks);
protected:
	void UpdateMatrix();

	LPDIRECT3DINDEXBUFFER9 _indexes;
	LPDIRECT3DDEVICE9 _device;
	LPDIRECT3DVERTEXBUFFER9 _buffer;
	
	D3DXVECTOR3 _rotation;
	D3DXVECTOR3 Rotation() const { return _rotation; }
	void Rotation(D3DXVECTOR3 val) { _rotation = val; UpdateMatrix();}

	D3DXVECTOR3 _position;
	D3DXVECTOR3 Position() const { return _position; }
	void Position(D3DXVECTOR3 val) { _position = val; UpdateMatrix();}

	D3DXMATRIX _rotationMatrix;
	D3DXMATRIX _world;
};

