#pragma once
class Camera
{
public:
	Camera(D3DXVECTOR3 pos, D3DXVECTOR3 lookat);
	~Camera(void);

	void Update(unsigned int ticks);

	D3DXMATRIX View() const { return _view; }
	D3DXMATRIX Projection() const { return _projection; }

	D3DXVECTOR3 Position() const { return _position; }
	void Position(D3DXVECTOR3 val) { _position = val; UpdateMatrix(); }

	D3DXVECTOR3 Lookat() const { return _lookat; }
	void Lookat(D3DXVECTOR3 val) { _lookat = val; UpdateMatrix(); }
protected:
	void UpdateMatrix();

	D3DXMATRIX _view;
	D3DXVECTOR3 _position;
	D3DXVECTOR3 _lookat;
	D3DXVECTOR3 _up;
	D3DXMATRIX _projection;
};

