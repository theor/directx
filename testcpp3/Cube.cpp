#include "StdAfx.h"
#include "Cube.h"
#include "Core\Inputs.h"


Cube::Cube() : Entity()
{
}


Cube::~Cube(void)
{
}

void Cube::Update( unsigned int ticks )
{
	if(Inputs::IsPressed(VK_CONTROL))
	{
		if(Inputs::IsPressed(VK_RIGHT))
			_position.y	+=0.05f;
		if(Inputs::IsPressed(VK_LEFT))
			_position.y	-=0.05f;
		if(Inputs::IsPressed(VK_UP))
			_position.x	+=0.05f;
		if(Inputs::IsPressed(VK_DOWN))
			_position.x	-=0.05f;
	}
	else
	{
		if(Inputs::IsPressed(VK_RIGHT))
			_rotation.y	+=0.05f;
		if(Inputs::IsPressed(VK_LEFT))
			_rotation.y	-=0.05f;
		if(Inputs::IsPressed(VK_UP))
			_rotation.x	+=0.05f;
		if(Inputs::IsPressed(VK_DOWN))
			_rotation.x	-=0.05f;
	}
	__super::Update(ticks);
}

void Cube::LoadContent(LPDIRECT3DDEVICE9 device)
{
	__super::LoadContent(device);
	D3DXMatrixTranslation(&_world, 12.0f, 4.0f, 0.0f);
	//D3DXMatrixIdentity(&_world);
	// tell Direct3D about our matrix

	CUSTOMVERTEX vertices[] =
	{
		{-1.0f, 1.0f,-1.0f,  D3DCOLOR_COLORVALUE( 1.0, 0.0, 0.0, 1.0 ) }, // 0
		{ 1.0f, 1.0f,-1.0f,  D3DCOLOR_COLORVALUE( 0.0, 1.0, 0.0, 1.0 ) }, // 1
		{-1.0f,-1.0f,-1.0f,  D3DCOLOR_COLORVALUE( 0.0, 0.0, 1.0, 1.0 ) }, // 2
		{ 1.0f,-1.0f,-1.0f,  D3DCOLOR_COLORVALUE( 1.0, 1.0, 0.0, 1.0 ) }, // 3
		{-1.0f, 1.0f, 1.0f,  D3DCOLOR_COLORVALUE( 1.0, 0.0, 1.0, 1.0 ) }, // 4
		{-1.0f,-1.0f, 1.0f,  D3DCOLOR_COLORVALUE( 0.0, 1.0, 1.0, 1.0 ) }, // 5
		{ 1.0f, 1.0f, 1.0f,  D3DCOLOR_COLORVALUE( 1.0, 1.0, 1.0, 1.0 ) }, // 6
		{ 1.0f,-1.0f, 1.0f,  D3DCOLOR_COLORVALUE( 1.0, 0.0, 0.0, 1.0 ) }  // 7
	};

	// create a vertex buffer interface called v_buffer
	device->CreateVertexBuffer(8*sizeof(CUSTOMVERTEX),
		0,
		CUSTOMFVF,
		D3DPOOL_MANAGED,
		&this->_buffer ,
		NULL);

	VOID* pVoid;    // a void pointer

	// lock v_buffer and load the vertices into it
	this->_buffer->Lock(0, 0, (void**)&pVoid, 0);
	memcpy(pVoid, vertices, sizeof(vertices));
	this->_buffer->Unlock();

	WORD g_cubeIndices[] =
	{
		0, 1, 2, 3, // Quad 0
		4, 5, 6, 7, // Quad 1
		4, 6, 0, 1, // Quad 2
		5, 2, 7, 3, // Quad 3
		1, 6, 3, 7, // Quad 4
		0, 2, 4, 5  // Quad 5
	};

	device->CreateIndexBuffer( 24*sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_DEFAULT,
		&_indexes,
		NULL );
	WORD *pIndices = NULL;

	_indexes->Lock( 0, sizeof(g_cubeIndices), (void**)&pIndices, 0 );
	memcpy( pIndices, g_cubeIndices, sizeof(g_cubeIndices) );
	_indexes->Unlock();

}

void Cube::Draw( LPDIRECT3DDEVICE9 device ){
	device->SetStreamSource(0, _buffer, 0, sizeof(CUSTOMVERTEX));

	device->SetIndices( _indexes );

	device->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, 0, 0, 8,  0, 2 );
	device->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, 0, 0, 8,  4, 2 );
	device->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, 0, 0, 8,  8, 2 );
	device->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, 0, 0, 8, 12, 2 );
	device->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, 0, 0, 8, 16, 2 );
	device->DrawIndexedPrimitive( D3DPT_TRIANGLESTRIP, 0, 0, 8, 20, 2 );
}
